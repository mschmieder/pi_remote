import re
import time

from pi_lirc.irsend import *

class IrControl:
    def __init__(self, name):
        self._name = name
        self._commands = []

    def name(self):
        return self._name.split('.')[0]

    def commands(self):
        if len(self._commands) is 0:
            ret = irsend(['LIST', self._name, ''])
            codes = ret.split('\n')
            for code in codes:
                match = re.match(r'irsend:.*\s(\w+)$', code)
                if match is not None:
                    self._commands.append(match.group(1))

        return self._commands

    def send_command(self, command, duration_in_sec=None, repeat=1, delay_in_sec=None):
        if duration_in_sec is None:
            # obviously we need to SEND_ONCE (multiple times)
            for i in range(0, repeat, 1):
                self.send_once(command)
                if delay_in_sec is not None:
                    time.sleep(delay_in_sec)
        else:
            self.send_start(command)
            time.sleep(duration_in_sec)
            self.send_stop(command)

    def send_start(self, command):
        if command not in self._commands:
            raise Execption("Unknown command " + command)

        return irsend(['SEND_START', self._name, command])

    def send_stop(self, command):
        if command not in self._commands:
            raise Execption("Unknown command " + command)

        return irsend(['SEND_STOP', self._name, command])

    def send_once(self, command):
        if command not in self._commands:
            raise Execption("Unknown command " + command)

        return irsend(['SEND_ONCE', self._name, command])