import subprocess
import re

from pi_lirc.ircontrol import *
from pi_lirc.irsend import *

class Lirc:
    def __init__(self):
        self._remotes = []

    def init(self):
        self._remotes = self._get_list_of_remotes()

    def get_remotes(self):
        return self._remotes

    @staticmethod
    def _get_list_of_remotes():
        lst_remotes = []
        # call irsend list to get all registered remotes
        ret = irsend(['LIST', "", ""])

        lines = ret.split('\n')

        for line in lines:
            match = re.match(r'irsend:\s+(.*)', line)
            if match is not None:
                lst_remotes.append(IrControl(name=match.group(1)))

        return lst_remotes