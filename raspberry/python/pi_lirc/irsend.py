import subprocess

def irsend(args=[]):
    command = ['irsend']
    command.extend(args)

    proc = subprocess.Popen(command,
                            stdout=subprocess.PIPE,
                            stderr=subprocess.PIPE)

    ret = proc.communicate()[1]
    return ret.decode('utf-8')