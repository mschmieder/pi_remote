import sys
import logging

from flask import Flask, request, jsonify


# import lirc module
import pi_lirc

class LircServer:
    def __init__(self):
        self._remote_commands = {}
        self._lirc = None
        self._app = Flask(__name__)

    def init(self):
        #self._lirc = pi_lirc.lirc.Lirc()
        #self._lirc.init()
        #lst_remotes = self._lirc.get_remotes()
        #   for remote in lst_remotes:
        #       self._remote_commands[remote.name()] = remote.commands()

        # add routes
        self._add_api_routes()

    def run(self):
        self._app.run(host='0.0.0.0', debug=True)


    def _add_api_routes(self):
        # test comment
        @self._app.route('/api/add_message/<uuid>', methods=['GET', 'POST'])
        def add_message(uuid):
            content = request.get_json(silent=True)
            print(content['mytext'])
            return jsonify({"uuid": uuid})


def main():
    ############################
    # SETUP LOGGING
    ############################
    logger = logging.getLogger()
    handler = logging.StreamHandler()
    formatter = logging.Formatter('%(asctime)s %(name)-12s %(levelname)-8s %(message)s')
    handler.setFormatter(formatter)
    logger.addHandler(handler)
    logger.setLevel(logging.DEBUG)

    server = LircServer()

    logger.info("Initializing Server...")
    server.init()
    logger.info("Initializing Server...Done")

    logger.info("Activating Server...")
    server.run()

    return 0


if __name__ == '__main__':
    try:
        ret = main()
    except:
        sys.exit(-1)

    sys.exit(ret)



